import React from 'react';
import ReactDOM from 'react-dom';
import App from './app/components/App';
import * as serviceWorker from './serviceWorker';
import {createStore,applyMiddleware} from 'redux'
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
//Types
import "./app/types/store/Store";
//Store
import Store from "./app/reducers/Store";


const store : Store = createStore(Store,applyMiddleware(thunk));

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>
    , document.getElementsByTagName('body')[0]);
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
