//@flow
import {WebSocketReducer} from "./reducers/websocketReducer/WebSocketReducerType";
import {ActiveRecorderReducerType} from "./reducers/recorderReducers/ActiveRecorderReducerType";
import {ComponentNavigatorReducerType} from "./reducers/componentNavigator/ComponentNavigatorReducerType";
import {RecordDetailsReducerType} from "./reducers/recordDetails/RecordDetailsReducerType";

export type Store = {
    webSocketReducer:WebSocketReducer,
    activeRecorderReducer:ActiveRecorderReducerType,
    componentNavigatorReducer:ComponentNavigatorReducerType,
    recordDetailsReducer:RecordDetailsReducerType,
}