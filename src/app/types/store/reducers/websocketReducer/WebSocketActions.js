import {StompClient} from "@stomp/stompjs";

export type AddSockJsRef = {
    type:string,
    payload:StompClient,
}
export type ConnectionStatus = {
    type:string,
    payload:{
        bool:Boolean,
        response:String
    }
}