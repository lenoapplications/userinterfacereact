//@flow

import {Client} from "@stomp/stompjs";

export const WebSocketReducer = {
    socketConfiguration: {
        url:String,
        port:Number,
    },
    socketStatus: {
        connectedToServer: Boolean,
        connectResponseFromServer:String,
    },
    sockJs: {
        clientSocket: Client,
    }
}