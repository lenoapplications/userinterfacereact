import type {DocumentEventDto} from "../../../dto/serializableEvents/DocumentEventDto";
import type {WindowEventDto} from "../../../dto/serializableEvents/WindowEventDto";

export type NewEventAssertPayload = {
    type:string,
    eventToAssert:{
        index:Number,
        eventObject:DocumentEventDto | WindowEventDto,
    }
}

