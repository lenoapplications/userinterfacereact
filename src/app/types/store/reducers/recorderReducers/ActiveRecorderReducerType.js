//@flow
import type {DocumentDto} from "../../../dto/serializableEvents/DocumentEventDto";
import type {WindowEventDto} from "../../../dto/serializableEvents/WindowEventDto";

export const ActiveRecorderReducerType = {
    status:{
        recording:Boolean,
    },
    events:{
        listOfCurrentEvents:Array,
        assertions: {
            eventToAssert:{
                index:Number,
                eventObject:Object,
            }
        }
    }
};


