//@flow
export type ComponentNavigatorReducerType = {
    recorder:{
        recorderHome:{
            recorderBody:{
                panel:{
                    panelDisplay:""
                },
                navigation:{
                    navigationDisplay:""
                }
            }
        }
    }
}