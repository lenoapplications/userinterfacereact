//@flow
export type  ContextMenuConfigParamsType ={
    options:[],
    displayConfig:{
        x:Number,
        y:Number,
    }
}
export type ContextMenuOptionType = {
    callback:Function,
    text:String,
}