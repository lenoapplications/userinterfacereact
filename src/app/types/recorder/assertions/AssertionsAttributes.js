export type AssertionAttributeType = {
    assertion:string,
}
export type ElementToAssertWith = {
    dto:Object,
    index:Number,
    assertionAttributes:Array,
}
export type HtmlPageViewerParentDivStyleParam = {
    marginLeft:Number,
}
