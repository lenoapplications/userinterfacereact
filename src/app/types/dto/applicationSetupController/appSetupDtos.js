//@flow


export type TestRecordConfigurationDto = {
    url:string,
    recordName:string,
    browser:string,
}