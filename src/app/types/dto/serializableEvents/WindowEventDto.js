export type WindowEventDto = {
    event:string,
    eventOn:string,
    windowName:string,
    assertionSerializableEvents:Object,
}