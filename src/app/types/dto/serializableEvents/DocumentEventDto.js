//@flow
export type DocumentDto = {
    event:string,
    eventOn:string,

    xpath:string,
    value:string,
    tag:string,
    assertionSerializableEvents:Object,

}