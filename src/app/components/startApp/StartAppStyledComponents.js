//@flow
import styled from 'styled-components'
export const StartAppDiv = styled.div
    `
    display:flex;
    flex-direction:row;
    width:100%;
    height:100%;
    `;


export const ConnectDiv = styled.div
    `
    display:flex;
    width:50%;
    height:50%;
    margin:auto;
    flex-direction:column;
    align-items:center;
    justify-content:center;
    `;


export const FormDiv = styled.div
    `
    width:80%;
    display:flex;
    flex-direction:column;
    margin:auto;
    align-items:center;
    `;

export const UrlLabel = styled.label
    `
    margin-bottom:2%;
    
    `;

export const PortLabel = styled.label
    `
    margin-top:2%;
    margin-bottom:2%;
    `;

export const UrlInput = styled.input
    `
    text-align:center;
    width:40%;
    `;
export const PortInput = styled.input
    `
    text-align:center;
    width:20%;
    `;