//@flow
//Core
import React,{Component} from "react";
import styled from 'styled-components';
import { connect } from 'react-redux';
import {BrowserRouter, Switch} from "react-router-dom";
import {Route} from "react-router";
import {bindActionCreators} from 'redux';
import type {Store} from "../types/store/Store";

//Functions
import {connectToServer,startApplication} from "./StartAppBehavior";


//StyledComponents
import {StartAppDiv,ConnectDiv,FormDiv,PortInput,UrlInput,PortLabel,UrlLabel} from "./StartAppStyledComponents";
import {WebSocketConf} from "../../../env/webSocketConf/WebSocketConf";



class StartApp extends Component{
    constructor(props){
        super(props);
        this.state = {}
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if(nextProps.sockJs != undefined){
            nextProps.start(nextProps.sockJs);
        }
        return nextProps;
    }


    render(): React.ReactNode {
        const string = `"<div class="sc-kafWEX gNlQuY"><div class="sc-bdVaJa eSkyPv"><div class="sc-bwzfXH flWccV"><div class="sc-htpNat iHSWJv"><label class="label label-primary label label-primary bg-secondary text-primary sc-bxivhb hSMzhs" for="connectDivUrlInput">URL:</label><input class="form-input sc-EHOje gMDqYk" id="connectDivUrlInput" disabled="" value="ws://localhost:8080/react"><label class="label label-primary bg-secondary text-primary sc-ifAKCX cJEScD" for="connectDivPortInput">PORT:</label><input class="form-input sc-bZQynM kLHzLO" id="connectDivPortInput" disabled="" value="8080"></div><button class="btn">Click to connect</button></div></div></div><script src="/main.60d5189edc3ba77f32a6.hot-update.js"></script>`;
        var a = new DOMParser();
        const b = a.parseFromString(string,"text/html");
        console.log(b.To);

        return(
            <StartAppDiv>
                <ConnectDiv>
                    <FormDiv>
                        <UrlLabel className ={"label label-primary label label-primary bg-secondary text-primary"} htmlFor = {"connectDivUrlInput"}>URL:</UrlLabel>
                        <UrlInput className ={"form-input"}id = {"connectDivUrlInput"} disabled = {"disabled"} value = {this.props.url}/>
                        <PortLabel className = {"label label-primary bg-secondary text-primary"} htmlFor = {"connectDivPortInput"}>PORT:</PortLabel>
                        <PortInput className = {"form-input"}id = {"connectDivPortInput"} disabled = {"disabled"} value = {this.props.port}/>
                    </FormDiv>

                    <button className={"btn"} onClick={()=>{this.props.connect(this.props.url)}}>Click to connect</button>
                </ConnectDiv>

            </StartAppDiv>
        )
    }
}

function mapStateToProps(store:Store){
    return{
        connectionStatus:store.webSocketReducer.socketStatus.connectedToServer,
        url:store.webSocketReducer.socketConfiguration.url,
        port:store.webSocketReducer.socketConfiguration.port,
        sockJs:store.webSocketReducer.sockJs.clientSocket,
    }
}
function mapDispatchToProps(dispatch : Function){
    return {
        connect:(url)=>{connectToServer(dispatch,url)},
        start:(client)=>{startApplication(client)},
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(StartApp);