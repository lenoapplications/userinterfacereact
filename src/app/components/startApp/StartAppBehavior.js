//@flow
import {WebSocketConf} from "../../../env/webSocketConf/WebSocketConf";
import webSocketActionTypes from "../../reducers/webSocket/actions/ActionsTypes";
import {Client} from '@stomp/stompjs'
import {ConnectionStatus} from '../../types/store/reducers/websocketReducer/WebSocketActions';


export function connectToServer(dispatch:Function,url:string){
    const client = new Client();
    client.configure({
        brokerURL:url,
        onConnect:()=>{
            setupStartApplicationSubscribition(dispatch,client);
            dispatchClientToStore(dispatch,client);}
    })
    client.activate();

}

export function startApplication(client:Client){
    client.publish({destination:WebSocketConf.sockJs.topics.controllers.applicationSetupController.request.startApplication,body:"Start application"})
}




function setupStartApplicationSubscribition(dispatch:Function,client:Client){
    client.subscribe(WebSocketConf.sockJs.topics.controllers.applicationSetupController.response.startApplicationResponse,(message)=>{
        const response = JSON.parse(message.body);
        dispatchConnectionStatus(dispatch,response);
    });
}
function dispatchClientToStore(dispatch:Function,client:Client){
    dispatch({
        type: webSocketActionTypes.connect.ADD_SOCKJS_REF,
        payload: client
    })
}
function dispatchConnectionStatus(dispatch:Function,response:Object){
    const actionConnectionStatus : ConnectionStatus = {
        bool:(response.response === "Connected")?true:false,
        response:response.response,
    }
    dispatch({
        type: webSocketActionTypes.connect.CHANGE_CONNECTION_STATUS,
        payload: actionConnectionStatus,
    })
}