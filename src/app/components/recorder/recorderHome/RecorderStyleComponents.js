//@flow
import styled from 'styled-components'


export const RecorderHomeDiv = styled.div
    `
    display : grid;
    width : 100%;
    height : 100%;
    grid-template-columns:1fr
    grid-template-rows:0.5fr 6.5fr 0.3fr
    grid-template-areas : 
    "Header"
    "Body"
    "Footer"
    `;



export const RecorderHomeHeader = styled.div
    `
    overflow:hidden;
    grid-area : Header;
    background-color:red;
    `;

export const RecorderHomeBody = styled.div
    `
    overflow:hidden;
    grid-area : Body;
    `;
export const RecorderHomeFooter = styled.div
    `
    overflow:hidden;
    grid-area : Footer;
    background-color:purple;
    `;