//@flow
//Core
import React,{Component} from "react";
import styled from 'styled-components';
import { connect } from 'react-redux';
import {BrowserRouter, Switch} from "react-router-dom";
import {Route} from "react-router";
import {bindActionCreators} from 'redux';
import type {Store} from "../../../types/store/Store";


//Components
import Body from "./recorderBody/RecorderBody";


//styledComponents
import {RecorderHomeDiv,RecorderHomeBody,RecorderHomeFooter,RecorderHomeHeader} from "./RecorderStyleComponents";

//Functions
import {} from "./RecorderHomeBehavior";






class RecorderHome extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    static getDerivedStateFromProps(nextProps, prevState) {

        return nextProps;
    }

    render(): React.ReactNode {
        return (
            <RecorderHomeDiv>
                <RecorderHomeHeader>

                </RecorderHomeHeader>

                <RecorderHomeBody>
                    <Body/>
                </RecorderHomeBody>

                <RecorderHomeFooter>

                </RecorderHomeFooter>

            </RecorderHomeDiv>
        );
    }
}


function mapStateToProps(store:Store){
    return{

    }
}
function mapDispatchToPropst(dispatch:Function){
    return {

    }
}


export default connect(mapStateToProps,mapDispatchToPropst)(RecorderHome)