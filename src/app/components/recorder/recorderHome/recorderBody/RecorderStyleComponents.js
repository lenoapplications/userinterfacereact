//@flow
import styled from 'styled-components'



export const RecorderBodyDiv =styled.div
    `
    display:grid;
    width:100%;
    height:100%;
    grid-template-columns:0.9fr 2.5fr 0.6fr
    grid-template-rows : 0.1fr 1.8fr;
    
    grid-template-areas :
    "RecordBar RecordBar RecordBar"
    "RecordNavigation RecordPanel RecordConfiguration"
    `;


export const RecordBar =styled.div
    `
    display:flex;
    flex-direction: column;
    grid-area : RecordBar;
    background-color:white;
    justify-content:center;
    align-items:flex-end;
    overflow:scroll;
    `;

export const NewTestRecordButton = styled.button
    `
    width : 10%;
    height: 50%;
    font-size: 10px;
    justify-content: flex-end;
    `;


export const RecorderNavigationDiv = styled.div
    `
    grid-area : RecordNavigation
    `;

export const RecordPanelDiv = styled.div
    `
    overflow:hidden;
    grid-area : RecordPanel     
    `;


export const RecordConfigurationDiv = styled.div
    `
    overflow:hidden;
    grid-area : RecordConfiguration
    background-color:grey;
    `;