//@flow
import {Client} from '@stomp/stompjs'
import React,{Component} from "react";

//Components
import EventRecorder from './blocks/panel/eventRecordingPanel/EventRecordingPanel';
import RecordDetail from './blocks/panel/recordDetailPanel/RecordDetailPanel';
import ActiveRecorderActionTypes from "../../../../reducers/recorder/activeRecording/actions/ActiveRecorderActionTypes";
import RecordExplorer from './blocks/navigationDiv/recordsExplorer/RecordsExplorer';

//Types
import NavigatorReducerActionTypes from '../../../../reducers/componentNavigator/actions/ComponentNavigatorActionTypes'



export function navigationDivSwitch(){
    switch(this.props.navigator.navigation.navToDisplay){
        case "RecordsExplorer":return <RecordExplorer/>
    }
}

export function panelSwitch(){
    switch (this.props.navigator.panel.panelToDisplay) {
        case "EventRecorder":{
            return(<EventRecorder/>);
        }
        case "RecordDetail":{
            return ( <RecordDetail/> )
        }
    }
}




export function dispatchRecorderActivated(){
        this.props.recorder.activeRecorder.recorderActivated();
}


export function dispatchActiveRecorderStatus(dispatch:Function,status:Boolean){
        dispatch({
            type:ActiveRecorderActionTypes.recordingInit.RECORDING_ACTIVATED,
            payload:status,
        })
}

export function changePanelDisplay(dispatch:Function,whatToDisplay:string){

}

export function testRecordDialog(){
    const displayStatus = (this.state.viewState.newTestRecordDialog !== true);
    this.setState({...this.state,viewState:{
                        ...this.state.viewState,newTestRecordDialog:displayStatus
        }});
}