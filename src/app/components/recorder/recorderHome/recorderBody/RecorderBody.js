//@flow
//Core
import React,{Component} from "react";
import styled from 'styled-components';
import { connect } from 'react-redux';
import {BrowserRouter, Switch} from "react-router-dom";
import {Route} from "react-router";
import {bindActionCreators} from 'redux';
import type {Store} from "../../../types/store/Store";
import AssertionDialog from "./blocks/panel/eventRecordingPanel/assertionDialog/AssertionDialog";


//Components
import RecordingDialog from './blocks/recordingDialog/RecordingDialog';

//Styled components
import {RecorderBodyDiv,RecordConfigurationDiv,RecorderNavigationDiv,RecordPanelDiv,RecordBar,
NewTestRecordButton} from "./RecorderStyleComponents";


//Functions
import {testRecordDialog,changePanelDisplay,panelSwitch,dispatchActiveRecorderStatus,dispatchRecorderActivated,
navigationDivSwitch} from "./RecorderBodyBehavior";



class RecorderBody extends Component{

    constructor(props){
        super(props);
        this.state = {
            viewState:{
                newTestRecordDialog:false,
            }
        };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return nextProps;
    }


    render(): React.ReactNode {
        return(
            <RecorderBodyDiv>
                    <RecordingDialog display={this.state.viewState.newTestRecordDialog}
                                     recorderActivated = {()=>{this.props.recorder.activeRecorder.recorderActivated()}}
                                     turnOff = {testRecordDialog.bind(this)}/>
                <RecordBar>
                    <NewTestRecordButton onClick = {testRecordDialog.bind(this)}>New Test Record</NewTestRecordButton>
                </RecordBar>

                <RecorderNavigationDiv>
                    {navigationDivSwitch.bind(this)()}
                </RecorderNavigationDiv>

                <RecordPanelDiv>
                    {panelSwitch.bind(this)()}
                </RecordPanelDiv>

                <RecordConfigurationDiv>

                </RecordConfigurationDiv>

            </RecorderBodyDiv>
        );
    }
}


function mapStateToProps(store:Store){
    return{
        navigator:{
            panel:{
                panelToDisplay:store.componentNavigatorReducer.recorder.recorderHome.recorderBody.panel.panelDisplay,
            },
            navigation:{
                navToDisplay:store.componentNavigatorReducer.recorder.recorderHome.recorderBody.navigation.navigationDisplay,
            }
        },
        recorder:{
            activeRecorder:{
                status:store.activeRecorderReducer.status.recording,
            }
        }
    }
}
function mapDispatchToProps(dispatch:Function){
    return {
        recorder:{
            activeRecorder: {
                recorderActivated:()=>{dispatchActiveRecorderStatus(dispatch,true)}
            }
        }
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(RecorderBody)