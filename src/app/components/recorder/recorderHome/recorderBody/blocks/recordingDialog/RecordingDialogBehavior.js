//@flow
import {WebSocketConf} from "../../../../../../../env/webSocketConf/WebSocketConf";
import {TestRecordConfigurationDto} from "../../../../../../types/dto/applicationSetupController/appSetupDtos";
import {Client} from '@stomp/stompjs'
import NavigatorReducerActionTypes
    from "../../../../../../reducers/componentNavigator/actions/ComponentNavigatorActionTypes";

export function setupSubscriptionTo(client:Client){
    client.subscribe(WebSocketConf.sockJs.topics.controllers.applicationSetupController.response.prepareForRecordingResponse,(message)=>{
        const response = JSON.parse(message.body);
        if (response.response === "Recording started"){
            this.props.displayEventRecorder();
            this.props.turnOff();
            this.props.recorderActivated();
        }
    });
}

export function prepereWebDriverForNewTestRecording(event,client:Client){
    event.preventDefault();
    const testRecordConfig : TestRecordConfigurationDto = this.state.configuration;
    const json = JSON.stringify(testRecordConfig);
    client.publish({destination:WebSocketConf.sockJs.topics.controllers.applicationSetupController.request.prepareForRecording,body:json})
}

export function handleOnChangeUrl(event){
    this.setState({...this.state,configuration: {
                        ...this.state.configuration,url: event.target.value
            }
        }
    );
}
export function handleOnChangeBrowser(event){
    this.setState({...this.state,configuration: {
                        ...this.state.configuration,browser: event.target.value
            }});
}
export function dispatchActionChangePanelDisplayToEventRecorder(dispatch:Function){
    dispatch({
        type:NavigatorReducerActionTypes.recorder.recorderHome.recorderBody.panel.changePanelDisplay,
        payload:"EventRecorder",
    })
}