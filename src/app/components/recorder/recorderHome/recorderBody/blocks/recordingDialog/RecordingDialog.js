//@flow
//Core
import React,{Component} from "react";
import styled from 'styled-components';
import { connect } from 'react-redux';
import {BrowserRouter, Switch} from "react-router-dom";
import {Route} from "react-router";
import {bindActionCreators} from 'redux';
import type {Store} from "../../../../../../types/store/Store"

//Functions
import {prepereWebDriverForNewTestRecording,handleOnChangeBrowser,handleOnChangeUrl,setupSubscriptionTo,
dispatchActionChangePanelDisplayToEventRecorder} from "./RecordingDialogBehavior";

//Styled components
import {RecordConfigurationDiv,SubmitNewTestRecording,BrowserWebDriverSelectDiv,CloseDialogButton,createRecordingDiv,BrowserWebDriverSelect,CloseDialogDiv
,ConfigurationForm,TestRecordUrlSite,TestRecordUrlSiteInput} from "./RecordingDialogStyleComponents";
import ActionsTypes from "../../../../../../reducers/webSocket/actions/ActionsTypes";
import type {Action} from "../../../../../../types/store/reducers/Action";


class RecordingDialog extends Component{

    constructor(props){
        super(props);
        this.state={
            configuration:{
                url:"http://eaapp.somee.com/Account/Login",
                recordName:"",
                browser:"Select browser:"
            }
        };
        setupSubscriptionTo.bind(this)(this.props.sockJs);
    }


    render(): React.ReactNode {
        const RecordingDialogDiv = createRecordingDiv((this.props.display)?"flex":"none");
        return(
            <RecordingDialogDiv>
                <RecordConfigurationDiv>
                    <ConfigurationForm onSubmit={(event)=>{prepereWebDriverForNewTestRecording.bind(this)(event,this.props.sockJs)}}>

                        <TestRecordUrlSite htmlFor = {"dialogTestRecord_urlSite"}>Url :</TestRecordUrlSite>
                        <TestRecordUrlSiteInput placeholder = {this.state.configuration.url} onBlur ={handleOnChangeUrl.bind(this)} id = {"dialogTestRecord_urlSite"}/>

                        <BrowserWebDriverSelectDiv>
                            <BrowserWebDriverSelect onChange = {handleOnChangeBrowser.bind(this)}>
                                <option value="0">{this.state.configuration.browser}</option>
                                <option value="chrome">Chrome</option>
                                <option value="firefox">FireFox</option>
                            </BrowserWebDriverSelect>
                        </BrowserWebDriverSelectDiv>


                        <SubmitNewTestRecording type="submit" value = "Submit"/>

                    </ConfigurationForm>


                    <CloseDialogDiv>
                        <CloseDialogButton onClick={this.props.turnOff} className="btn-sm"><i className="icon icon-cross"/></CloseDialogButton>
                    </CloseDialogDiv>


                </RecordConfigurationDiv>

            </RecordingDialogDiv>
        );
    }

}


function mapStateToProps(store:Store){
    return{
        sockJs:store.webSocketReducer.sockJs.clientSocket,
    }
}
function mapDispatchToProps(dispatch:Function){
    return {
        displayEventRecorder:()=>{dispatchActionChangePanelDisplayToEventRecorder(dispatch)}
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(RecordingDialog);