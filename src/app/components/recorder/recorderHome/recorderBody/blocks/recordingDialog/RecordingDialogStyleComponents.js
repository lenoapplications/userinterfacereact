//@flow
import styled from 'styled-components'


export function createRecordingDiv (display){
    return  styled.div
        `
      display: ${display}; /* Hidden by default */
      position: fixed; /* Stay in place */
      z-index: 1; /* Sit on top */
      left: 0;
      top: 0;
      width: 100%; /* Full width */
      height: 100%; /* Full height */
      overflow: auto; /* Enable scroll if needed */
      background-color: rgb(0,0,0); /* Fallback color */
      background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    `;
};


export const RecordConfigurationDiv = styled.div
    `
    display:flex;
    width:50%;
    height:80%;
    margin:auto;
    flex-direction:column;
    `;

export const CloseDialogDiv = styled.div
    `
    display:flex
    width:100%;
    flex:0.1;
    margin-left:auto;
    margin-top:auto;
    
    `;
export const CloseDialogButton = styled.button
    `
    align-self:flex-end;
    margin-left:auto;
    margin-top:auto;
    `;

export const ConfigurationForm = styled.form
    `
    display:flex;
    width:100%;
    flex:2;    
    background-color:white;
    flex-direction:column;
    `;

export const TestRecordUrlSite = styled.label
    `
    width:25%;
    height:3%;
    font-size:70%;
    `;
export const TestRecordUrlSiteInput = styled.input
    `
    width:25%;
    height:3%;
    font-size:50%;
    `;

export const BrowserWebDriverSelectDiv = styled.div
    `
    height:10%;
    font-size:50%;
    `;
export const BrowserWebDriverSelect = styled.select
    `
    height:10%;
    margin-top:auto;
    `;

export const SubmitNewTestRecording = styled.input
    `
    width:10%;
    height:1%;
    font-size:50%;
    
    `;