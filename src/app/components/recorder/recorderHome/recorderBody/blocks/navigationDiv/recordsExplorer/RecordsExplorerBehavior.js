//@flow
import React from "react";
//assets
import folderPng from '../../../../../../../../assets/folder.png';
//type
import {RecordFolderStyleParam} from "../../../../../../../types/recorder/navigation/explorer/RecorderExplorerTypes";
//env
import {WebSocketConf} from "../../../../../../../../env/webSocketConf/WebSocketConf";

//Styled Components
import {
    TestRecordsDiv, RecordDiv, RecordName,
    RecordSubFolderDiv, SubFolderFilesDiv,
    SubFolderName, RecordsExplorerMainDiv,SubFolderNameAndImgDiv,
    SubFolderImg} from "./RecordsExplorerStyledComponents";

//Types
import NavigatorReducerActionTypes from "../../../../../../../reducers/componentNavigator/actions/ComponentNavigatorActionTypes";
import RecordDetailReducerActionTypes from "../../../../../../../reducers/recordDetails/actions/RecordDetailActionTypes";


export function getAllTestRecords(client:Client){
    client.publish({destination:WebSocketConf.sockJs.topics.controllers.records.recordsExplorer.request.getAllTestRecordsRequest,body:""});
    client.subscribe(WebSocketConf.sockJs.topics.controllers.records.recordsExplorer.response.getAllTestRecordsResponse,(message)=>{
        const response = JSON.parse(message.body);
        updateStateOfRecords.bind(this)(response.records);
    })
}

export function dispatchActionChangePanelDisplayToRecordDetail(path:string,dispatch:Function){
    dispatch({
        type:NavigatorReducerActionTypes.recorder.recorderHome.recorderBody.panel.changePanelDisplay,
        payload:"RecordDetail",
    });
    dispatch({
        type:RecordDetailReducerActionTypes.newFileRecordToDisplay,
        payload: path,
    })
}

export function renderAllTestRecords(){
    const styleParameter:RecordFolderStyleParam = {marginLeft:0};
    const listOfAllTestRecordsComponents = setupTestRecordComponents.bind(this)(styleParameter,this.state.records.recordInfoHolderHashMap,"/");
    return(
        <TestRecordsDiv>
            {listOfAllTestRecordsComponents.map((component)=>{return component})}
        </TestRecordsDiv>
    )
}




function setupTestRecordComponents(styleParameters:RecordFolderStyleParam,recordFolder:Object,path:string){
    const listOfComponents = [];
    for (const key in recordFolder){
        if (recordFolder.hasOwnProperty(key)){
            if (recordFolder[key].type === "folder"){
                const concatPath = path.concat(key).concat("/");
                listOfComponents.push(setupSubFolderRecordComponents.bind(this)(styleParameters,key,recordFolder[key],concatPath));
            }else if (recordFolder[key].type === "file" && key.includes(".rct")){
                listOfComponents.push(renderRecord.bind(this)(key,recordFolder[key],path));
            }
        }
    }
    return listOfComponents;
}

function setupSubFolderRecordComponents(styleParameters:RecordFolderStyleParam,folderName:string,recordSubFolder:Object,path:string){
    updateStyleFolderParameters(styleParameters);
    const subFolderAndFilesList = setupTestRecordComponents.bind(this)(styleParameters,recordSubFolder.recordInfoHolderHashMap,path);
    const SubFolderFilesComponent = SubFolderFilesDiv(styleParameters);
    const subComponent = (
        <RecordSubFolderDiv key = {folderName}>
            <SubFolderNameAndImgDiv onClick = {clickOnSubFolderRecord}>
                <SubFolderImg src={folderPng} alt={"folderLogo"}/>
                <SubFolderName>{folderName}</SubFolderName>
            </SubFolderNameAndImgDiv>

            <SubFolderFilesComponent>
                {subFolderAndFilesList.map((component)=>{return component;})}
            </SubFolderFilesComponent>
        </RecordSubFolderDiv>
    );
    return subComponent;
}

function renderRecord(recordName:string,recordFile:Object,path:string){
    const displayName = modifyRecordNameBeforeDisplaying(recordName);
    return(
        <RecordDiv onClick={()=>{this.props.displayRecordDetail(path.concat(recordName))}} key = {recordName}>
            <RecordName>{displayName}</RecordName>
        </RecordDiv>);
}



function modifyRecordNameBeforeDisplaying(recordName:string){
    if (recordName.length > 15){
        const newRecordPartOne = recordName.substr(0,15).replace(".rct","");
        return newRecordPartOne.concat("...rct");
    }
    return recordName;
}

function clickOnSubFolderRecord(event:Object){
    const subFilesDiv = event.target.parentElement.nextSibling;
    subFilesDiv.style.display = (subFilesDiv.style.display === "") ? "flex":"";
}
function updateStyleFolderParameters(styleParam:RecordFolderStyleParam){
    styleParam.marginLeft += 5;
}
function updateStateOfRecords(newRecords:Object){
    this.setState({...this.state,records:newRecords});
}
