//@flow
import styled from 'styled-components';
import type {RecordFolderStyleParam} from "../../../../../../../types/recorder/navigation/explorer/RecorderExplorerTypes";


//MAIN DIV
export const RecordsExplorerMainDiv = styled.div
    `
    display:flex;
    flex-direction:column;
    width:100%;
    height:100%;
    overflow:hidden;
    background-color:ghostwhite;
    `;
//ACTION BAR
export const ExplorerActionBar = styled.div
    `
    display:flex;
    width:100%;
    background-color:red;
    height:5%;
    margin-bottom:2px;
    `
//EXPLORER DIV
export const ExplorerDiv = styled.div
    `
    display:flex;
    width:100%;
    height:95%;
    margin-left:10px;
    overflow:hidden;
    `;
export const TestRecordsDiv = styled.div
    `
    display:flex:
    flex-direction:column;
    width:100%;
    height:100%;
    padding:10px;
    overflow:auto;
    `;
export const RecordSubFolderDiv = styled.div
    `
    display:flex;
    flex-direction:column;
    width:100%;
    height:auto;
    `;
export const SubFolderNameAndImgDiv = styled.div
    `
    display:flex;
    width:100%;
    height:auto;
    `;
export const SubFolderName = styled.div
    `
    width:100%;
    font-size:70%;
    font-weight:bold;
    margin-bottom:5px;
    color:MediumPurple;
    margin-left:3px;
    &:hover {
    cursor:default;
    }
    `;
export const SubFolderImg = styled.img
    `
    width:15px;
    height:15px;  
    `;
export const SubFolderFilesDiv =(styleParameters:RecordFolderStyleParam)=> {
    return styled.div`
    display:none;
    flex-direction:column;
    width:100%;
    height:auto;
    margin-left:${styleParameters.marginLeft.toString().concat("px")}
    `;
};
export const RecordDiv = styled.div
    `
    display:flex;
    width:100%;
    height:20px;
    margin-top:2px;
    overflow:hidden;
    `;
//RecordDivComponents
export const RecordName = styled.p
    `
    font-size:70%;
    font-weight:bold;
    color:Teal;
    &:hover {
    cursor:default;
    }
    `;

