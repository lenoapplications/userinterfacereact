//@flow
//Core
import React,{Component} from "react";
import { connect } from 'react-redux';
import {BrowserRouter, Switch} from "react-router-dom";
import {Route} from "react-router";
import {bindActionCreators} from 'redux';
import type {Store} from "../../../types/store/Store";
import {dispatchActiveRecorderStatus} from "../../../RecorderBodyBehavior";


//Styled Components
import {RecordsExplorerMainDiv,ExplorerDiv,
ExplorerActionBar} from "./RecordsExplorerStyledComponents";

//Functions
import {getAllTestRecords,renderAllTestRecords,
dispatchActionChangePanelDisplayToRecordDetail} from "./RecordsExplorerBehavior";

class RecordsExplorer extends Component{
    constructor(props){
        super(props);
        this.state = {
            records:{},
        };
        getAllTestRecords.bind(this)(this.props.sockJs);
    }


    render(): React.ReactNode {
        return (
            <RecordsExplorerMainDiv>
                <ExplorerActionBar>
                </ExplorerActionBar>
                <ExplorerDiv>
                    {renderAllTestRecords.bind(this)()}
                </ExplorerDiv>
            </RecordsExplorerMainDiv>
        );
    }
}





function mapStateToProps(store:Store){
    return{
        sockJs:store.webSocketReducer.sockJs.clientSocket,
    }
}
function mapDispatchToProps(dispatch:Function){
    return {
        displayRecordDetail:(path:string)=>{dispatchActionChangePanelDisplayToRecordDetail(path,dispatch)},
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(RecordsExplorer)