//@flow
import styled from "styled-components";


export function createAssertionDiv (display){
    return  styled.div
        `
      display: ${display}; /* Hidden by default */
      position: fixed; /* Stay in place */
      z-index: 1; /* Sit on top */
      left: 0;
      top: 0;
      width: 100%; /* Full width */
      height: 100%; /* Full height */
      overflow: auto; /* Enable scroll if needed */
      background-color: rgb(0,0,0); /* Fallback color */
      background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    `;
};


export const AssertionMainDiv = styled.div
    `
    display:flex;
    width:80%;
    height:90%;
    margin:auto;
    flex-direction:column;
    overflow:hidden;
    `;


export const AssertionActionBarDiv = styled.div
    `
    display:flex;
    width:100%;
    height:5%;
    overflow:bottom;
    border-bottom:1px outset ghostwhite;
    `;
export const AssertionActionBarButton = styled.button
    `
    font-weight:bold;
    font-size:60%;
    height:80%;
    text-align:center;
    background-color:ghostwhite;
    border-radius:20%;
    margin:2px;
    `;

export const AssertionForm = styled.div
    `
    display:flex;
    width:100%;
    height:90%;
    background-color:white;
    `;

export const ElementDiv = styled.div
    `
    display:flex;
    flex-direction:column;
    width:90%;
    height:10%;
    margin-top:10px;
    align-self:center;
    border:1px groove silver;
    padding:10px;
    `;
export const ElementHeaderDiv = styled.div
    `
    display:flex;
    width:100%;
    flex:1;
    `;
export const AssertElementParagraph = styled.p
    `
    font-weight:bold;
    font-size:60%;
    color:silver;
    `;
export const ElementBodyInfoDiv = styled.div
    `
    display:flex;
    width:100%;
    flex:1;
    `;
export const ElementTag = styled.p
    `
    font-weight:bold;
    font-size:50%;
    align-self:center;
    color:black;
    `;

export const ElementMainAssertConfigurationDiv = styled.div
    `
    display:flex;
    flex-direction:column;
    width:100%;
    height:90%;
    `;
export const ElementAssertConfigurationTypeSelectDiv = styled.div
    `
    display:flex;
    width:100%;
    height:10%;
    align-items:center;
    background-color:gray;
    `;
export const ElementAssertConfigurationSelect = styled.select
    `
    font-size:50%;
    width:25%;
    height:10%;
    `;
export const ElementAssertConfigurationOption = styled.option
    `
    `;

export const ElementAssertionConfListMainDiv = styled.div
    `
    display:flex;
    margin:5px;
    width:100%;
    height:10%;
    `;
export const ElementAssertionConfListTypeAssertion = styled.p
    `
    font-size:60%;
    font-weight:bold;
    color:gray;
    `;
export const ElementAssertionConfListTypeAssertionName = styled.p
    `
    font-size:50%;
    font-weight:bold;
    margin-left:2px;
    `;

export const CloseDialogDiv = styled.div
    `
    display:flex
    width:100%;
    height:4.5%;
    margin-left:auto;
    margin-top:auto;
    align-items:flex-start;
    `;
export const CloseDialogButton = styled.button
    `
    align-self:flex-end;
    margin-left:auto;
    margin-top:auto;
    `;
