//@flow
//Core
import React,{Component} from "react";
import styled from 'styled-components';
import {deactivateAssertEventOnServer} from "../../AssertionDialogBehavior";

//Dialogs
import {renderPageHtmlViewer} from "./dialogs/pageHtmlViewer/PageHtmlViewer";

//Functions
import {closeDialogCallbackFunction} from "./dialogs/pageHtmlViewer/PageHtmlViewer";

const DialogBoxDiv =styled.div
    `
      display: flex; /* Hidden by default */
      position: fixed; /* Stay in place */
      z-index: 10; /* Sit on top */
      left: 0;
      top: 0;
      align-items:center;
      justify-content:center;
      width: 100%; /* Full width */
      height: 100%; /* Full height */
      overflow: auto; /* Enable scroll if needed */
      background-color: rgb(0,0,0); /* Fallback color */
      background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    `;
const MainDiv = styled.div
    `
    display:flex;
    flex-direction:column;
    width:60%;
    height:80%;
    margin-left:20%;
    `;
const BoxDiv = styled.div
    `
    display:flex;
    width:100%;
    height:100%;
    background-color:black;
    `;

function dialogToDisplay(type:string){
    switch (type) {
        case "PageHtmlViewer":return [renderPageHtmlViewer.bind(this)(),closeDialogCallbackFunction.bind(this)];
    }
}

function closeDialog(target){
    if (target.id === "assertDialogBoxesIdClosePurpose"){
        this.setState({...this.state,dialogBox:{
                        active:false,
                        type:"",
                        dialogBoxState:{}}});
    }
}


export function renderDialogBox(){
    if (this.state.dialogBox.active){
        const dialogArray = dialogToDisplay.bind(this)(this.state.dialogBox.type);
        const dialogRendered = dialogArray[0];
        const dialogCloseCallback = dialogArray[1];
        return (
            <DialogBoxDiv id = {"assertDialogBoxesIdClosePurpose"} onClick={(event)=>{dialogCloseCallback();closeDialog.bind(this)(event.target);}}>
                <MainDiv>
                    <BoxDiv>
                        {dialogRendered}
                    </BoxDiv>
                </MainDiv>

            </DialogBoxDiv>)
    }
}