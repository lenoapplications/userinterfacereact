//@flow
//Core
import React,{Component} from "react";
import styled from 'styled-components';
import { connect } from 'react-redux';
import {BrowserRouter, Switch} from "react-router-dom";
import {Route} from "react-router";
import {bindActionCreators} from 'redux';
import {setupSubscriptionTo} from "../eventRecordingPanel/EventRecordingPanelBehavior";
import type {NewEventAssertPayload} from "../../../../../../../types/store/reducers/recorderReducers/ActiveRecorderActionsType";

//Functions

//Style Components
import {MainRecordDetailDiv} from "./RecordDetailStyleComponents";

//Components

class RecordDetailPanel extends Component{
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render(): React.ReactNode {
        console.log(this.props);
        return(
            <MainRecordDetailDiv>

            </MainRecordDetailDiv>
        );
    }

}




function mapStateToProps(store:Store){
    return{
        sockJs:store.webSocketReducer.sockJs.clientSocket,
        recordPath:store.recordDetailsReducer.path,
    }
}
function mapDispatchToProps(dispatch:Function){
    return {
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(RecordDetailPanel)