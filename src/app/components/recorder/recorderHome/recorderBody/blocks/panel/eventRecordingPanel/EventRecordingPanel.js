//@flow
//Core
import React,{Component} from "react";
import styled from 'styled-components';
import { connect } from 'react-redux';
import {BrowserRouter, Switch} from "react-router-dom";
import {Route} from "react-router";
import {bindActionCreators} from 'redux';
//Types
import type {NewEventAssertPayload} from "../../../../../../../types/store/reducers/recorderReducers/ActiveRecorderActionsType";

//Components
import AssertionDialog from "./assertionDialog/AssertionDialog";

//Functions
import {renderListOfEvents,setupSubscriptionTo,assertionDialog,saveRecord} from "./EventRecordingPanelBehavior";

//Stlyed components
import {EventRecordingPanelDiv,EventsDiv,EventsListDiv,EventsToolBarDiv,EventsToolBarSaveRecord} from "./EventRecordingPanelStyleComponents";




class EventRecordingPanel extends Component{
    constructor(props){
        super(props);
        this.state = {
            assertions:{
                assertionDialog:false,
                step:undefined,
            }
        };
        this.props.setupEventSubscription(this.props.sockJs);
    }

    render(): React.ReactNode {
        return (
            <EventRecordingPanelDiv>
                {this.state.assertions.assertionDialog &&
                    <AssertionDialog display={this.state.assertions.assertionDialog}
                                     step = {this.state.assertions.step}
                                     turnOff = {assertionDialog.bind(this)}/>
                }
                <EventsDiv>
                    <EventsToolBarDiv>
                        <EventsToolBarSaveRecord onClick={()=>{saveRecord(this.props.sockJs)}}>Save test record</EventsToolBarSaveRecord>
                    </EventsToolBarDiv>

                    <EventsListDiv>
                        {renderListOfEvents.bind(this)()}
                    </EventsListDiv>
                </EventsDiv>

            </EventRecordingPanelDiv>
        )
    }
}


function mapStateToProps(store:Store){
    return{
        recordingStatus:store.activeRecorderReducer.status.recording,
        eventsList:store.activeRecorderReducer.events.listOfCurrentEvents,
        sockJs:store.webSocketReducer.sockJs.clientSocket,
    }
}
function mapDispatchToProps(dispatch:Function){
    return {
        setupEventSubscription:(client:Client)=>{setupSubscriptionTo(dispatch,client)},
        newAssertionEvent:(eventAssertObject:NewEventAssertPayload)=>{dispatch(eventAssertObject)},
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(EventRecordingPanel);