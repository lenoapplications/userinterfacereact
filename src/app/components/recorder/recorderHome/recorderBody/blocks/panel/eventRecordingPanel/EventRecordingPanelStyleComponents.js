//@flow
import styled from 'styled-components'



export const EventRecordingPanelDiv = styled.div
    `
    display:flex;
    width:100%;
    height:100%;
    overflow:hidden;
    `;

export const EventsDiv = styled.div
    `
    display:flex;
    flex-direction:column;
    width:100%;
    height:100%;
    overflow:scroll;
    `;

export const EventsListDiv = styled.div
    `
    display:flex;
    flex-direction:column;
    align-items:center;
    margin-top:10px;
    margin-bottom:10px;
    height:90%;
    overflow:auto;
    `;
export const EventsToolBarDiv = styled.div
    `
    display:flex;
    flex-direction:column;
    width:100%;
    justify-content:center;
    height:10%;
    overflow:hidden;
    border:2px groove silver;
    `;
export const EventsToolBarSaveRecord = styled.button
    `
    font-size:65%;
    width:11%;
    margin-left:auto;
    align-self:center;
    `;
export const ListItemDiv = styled.div
    `
    display:flex;
    width:90%;
    height:17%;
    background-color:ghostwhite;
    flex-shrink:0;
    margin-bottom:10px;
    `;
export const ItemDivEventInfoDiv = styled.div
    `
    display:flex;
    flex-direction:column;
    flex:1;
    height:100%;
    overflow:hidden;
    `;
export const ItemDivEventProperties = styled.div
    `
    display:flex;
    flex:2;
    height:100%;
    overflow:hidden;
    `;
export const ItemDivEventAssertion = styled.div
    `
    display:flex;
    flex:1
    height:100%;
    overflow:hidden;
    `;
export const EventStep = styled.p
    `
    font-weight:bold;
    `;
export const EventInfo = styled.p
    `
    
    `;

export const AssertionButton = styled.button
    `
    
    `;

