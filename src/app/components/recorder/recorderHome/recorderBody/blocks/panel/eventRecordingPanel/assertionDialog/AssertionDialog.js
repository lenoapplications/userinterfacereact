//@flow
//Core
import React,{Component} from "react";
import styled from 'styled-components';
import { connect } from 'react-redux';
import {BrowserRouter, Switch} from "react-router-dom";
import {Route} from "react-router";
import {bindActionCreators} from 'redux';
import type {Store} from "../../../../../../../../types/store/Store";

//Styled components
import {createAssertionDiv,AssertionMainDiv,CloseDialogDiv,CloseDialogButton,AssertionForm
,AssertionActionBarDiv,AssertionActionBarButton} from "./AssertionDialogStyleComponents";




//Functions
import {activateAssertEventOnServer,deactivateAssertEventOnServer,changeAssertionType,
setupUnSubscriptionTo,renderAssertComponent} from './AssertionDialogBehavior';
import {renderDialogBox} from "./parts/dialogBox/DialogBox";


class AssertionDialog extends Component{
    constructor(props) {
        super(props);
        this.state = {
            dialogBox:{
                active:false,
                type:"PageHtmlViewer",
                dialogBoxState:{},
            },
            assertionType:"",
            status:{
              assertActivated : false,
            },
            assertEvents:{
                list:[],
            },
        };
        activateAssertEventOnServer.bind(this)(this.props.sockJs,this.props.assertionEvent);
    }
    componentWillUnmount(): void {
        setupUnSubscriptionTo(this.props.sockJs);
    }


    render(): React.ReactNode {
        const AssertionDialog = createAssertionDiv(this.props.display?"flex":"none");
        console.log(this.state);
        let base64Data;
        if (this.state.array !== undefined){
            base64Data = btoa(String.fromCharCode.apply(null, this.state.array));
            console.log(base64Data);
        }
        if (base64Data !== undefined ){
            return (<AssertionDialog>
                        <img alt = {"test"} src = {`data:image/png;base64,${base64Data}`}/>
            </AssertionDialog>)
        }else{
            return (
                <AssertionDialog>
                    {renderDialogBox.bind(this)()}
                    <AssertionMainDiv>
                        <AssertionActionBarDiv>
                            <AssertionActionBarButton onClick = {()=>{changeAssertionType.bind(this)("CODE")}}>CODE</AssertionActionBarButton>
                        </AssertionActionBarDiv>

                        <AssertionForm>
                            {renderAssertComponent.bind(this)()}
                        </AssertionForm>

                        <CloseDialogDiv>
                            <CloseDialogButton onClick={()=>{ deactivateAssertEventOnServer.bind(this)(this.props.sockJs) ; this.props.turnOff()}} className="btn-sm"><i className="icon icon-cross"/></CloseDialogButton>
                        </CloseDialogDiv>

                    </AssertionMainDiv>
                </AssertionDialog>
            );
        }
    }

}
function mapStateToProps(store:Store){
    return{
        sockJs:store.webSocketReducer.sockJs.clientSocket,
        assertionEvent:store.activeRecorderReducer.events.assertions.eventToAssert,

    }
}
function mapDispatchToPropst(dispatch:Function){
    return {
    }
}


export default connect(mapStateToProps,mapDispatchToPropst)(AssertionDialog);