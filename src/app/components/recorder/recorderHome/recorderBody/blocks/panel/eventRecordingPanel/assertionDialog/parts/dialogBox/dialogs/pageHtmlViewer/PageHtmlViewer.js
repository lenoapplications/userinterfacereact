//@flow
//Core
import React from "react";
import styled from 'styled-components';
//Functions
import {renderContextMenuComponent} from "../../../../../../../../../../../reusableComponents/contextMenu/ContextMenu";
import {renderByteImageDisplay} from "../../../../../../../../../../../reusableComponents/byteImageDisplay/ByteImageDisplay";



//Types
import {HtmlPageViewerParentDivStyleParam
} from "../../../../../../../../../../../../types/recorder/assertions/AssertionsAttributes";
//Env
import {WebSocketConf} from "../../../../../../../../../../../../../env/webSocketConf/WebSocketConf";
//Assets
import inspectLogo from "../../../../../../../../../../../../../assets/elementInspection.png";
import type {ContextMenuConfigParamsType} from "../../../../../../../../../../../../types/reusableComponent/ContextMenuTypes";
import type {ElementToAssertWith} from "../../../../../../../../../../../../types/recorder/assertions/AssertionsAttributes";
import type {DocumentDto} from "../../../../../../../../../../../../types/dto/serializableEvents/DocumentEventDto";
import {renderIsDisplayedComponentConf} from "../../../assertionComponent/isDisplayedComponent";



const PageHtmlViewer = styled.div
    `
    display:flex;
    flex-direction:column;
    width:100%;
    height:100%;
    background-color:white;
    overflow:hidden;
    border:2px solid gray;
    `;
const HtmlActionBar = styled.div
    `
    display:flex;
    width:100%;
    height:5%;
    border-bottom:1px solid gray;
    overflow:hidden;
    `;
const HtmlPreviewDiv = styled.div
    `
    display:flex;
    width:100%;
    height:95%;
    padding:5px;
    overflow:hidden;
    `;
const ElementPreviewDiv = styled.div
    `
    display:flex;
    flex-direction:column;
    width:40%;
    height:100%;
    border-right:1px solid gray;
    overflow:hidden;
    `;
const ElementPreviewImageDiv = styled.div
    `
    display:flex;
    width:100%;
    height:50%;
    overflow:hidden;
    `;
const ElementPreviewInfo = styled.div
    `
    display:flex;
    width:100%:
    height:20%;
    overflow:hidden;
    `;
const ElementPreviewTagName = styled.p
    `
    font-size:70%;
    &:hover {
    cursor:default;
    }
    `;
const BodyTreePreviewDiv = styled.div
    `
    display:flex;
    width:60%;
    height:100%;
    overflow:hidden;
    `;
const BodyDiv = styled.div
    `
    display:flex;
    flex-direction:column;
    width:100%;
    height:100%;
    padding:20px;
    overflow:auto;
    `;
const ParentElementDiv = (styleParam:HtmlPageViewerParentDivStyleParam)=>{
    return styled.div
        `
        display:flex;
        flex-direction:column;
        width:100%;
        height:auto;
        margin-left:${styleParam.marginLeft.toString().concat("px")};
        `
};
const ElementTagNameAndImgDiv = styled.div
    `
    display:flex;
    `;
const ElementTagName = styled.p
    `
    font-size:65%;
    color:blue;
    font-weight:bold;
     &:hover {
    cursor:default;
    }
    `;
const ParentChildrenDiv = (parent:Object)=>{
    return styled.div`
    display:${(parent.state.expandState.expanded)?"flex":"none"};
    flex-direction:column;
    width:100%;
    height:auto;
    `;
};
const InspectElementImg = styled.img
    `
    margin-right:5px;
    width:10px;
    height:12px;
    `;



function activatePageHtmlViewer(sockJs:Client){
    sockJs.publish({destination:WebSocketConf.sockJs.topics.controllers.assertionEventController.request.getDocumentBodyToStringRequest,body:""});
    sockJs.publish({destination:WebSocketConf.sockJs.topics.controllers.assertionEventController.request.getWebScreenRequest,body:""});
    subscribeOnDocumentBodyToString.bind(this)(sockJs);
    subscribeOnGetWebScreenShot.bind(this)(sockJs);
}
function subscribeOnDocumentBodyToString(sockJs:Client){
    sockJs.subscribe(WebSocketConf.sockJs.topics.controllers.assertionEventController.response.getDocumentBodyToStringResponse,(message)=>{
        const response = JSON.parse(message.body);
        const htmlDoc = new DOMParser().parseFromString(response.returnValue,"text/html");
        saveHtmlDocToState.bind(this)(htmlDoc);
    });
}
function subscribeOnGetWebScreenShot(sockJs:Client){
    sockJs.subscribe(WebSocketConf.sockJs.topics.controllers.assertionEventController.response.getWebScreenResponse,(message)=>{
        const binaryArray = new Uint8Array(message.binaryBody);
        this.updateByteDisplayArray(binaryArray);
    });
}
function saveHtmlDocToState(doc:Document){
    this.setState({...this.state,dialogBox:{
            ...this.state.dialogBox,dialogBoxState: {
                ...this.state.dialogBox.dialogBoxState,htmlDoc:doc,
                treeStatus:{
                    elements:setupTreeStatus(doc),
                },
                inspectElement:{
                    element:undefined,
                },
            }
        }
    })
}
function setupTreeStatus(doc:Document){
    const body = doc.body;
    const styleParameter = {marginLeft:0};
    const stateTracker = {index:-1};
    return setupTreeStatusRecursiveLoop(styleParameter,stateTracker,body);

}
function setupTreeStatusRecursiveLoop(styleParameter:HtmlPageViewerContextMenuDivStyleParam,stateTracker:Object,parent:Element){
    const updatedStyleParameter = updateHtmlStyleParameters(styleParameter);
    const childNodeList = [];
    const arrayOfElements = [...parent.children];
    for(let i = 0; i < arrayOfElements.length; ++i){
        const child:Element = arrayOfElements[i];
        const stateObjectElement = createStateElementObject(child,stateTracker,styleParameter);
        if (child.hasChildNodes()){
            stateObjectElement.state.childNodes = setupTreeStatusRecursiveLoop(updatedStyleParameter, stateTracker,child);
        }
        childNodeList.push(stateObjectElement);
    }
    return childNodeList;
}
function createStateElementObject(element:Element,stateTracker:Object,styleParameter:HtmlPageViewerParentDivStyleParam){
    stateTracker.index +=1;
    return{
        key:stateTracker.index,
        domElement:element,
        xpath:getXPathForElement(element),
        styleParameter:styleParameter,
        state:{
            hasChild:element.hasChildNodes(),
            expandState:{
                expandable:element.hasChildNodes(),
                expanded:false,
            },
            childNodes:[],
        }
    };
}



function renderInspectElement(){
    if (this.state.dialogBox.dialogBoxState.inspectElement.element !== undefined){
        return <ElementPreviewInfo>
                    <ElementTagName>{this.state.dialogBox.dialogBoxState.inspectElement.element.domElement.tagName}</ElementTagName>
               </ElementPreviewInfo>
    }
}
function renderHtmlDoc(){
    return <BodyDiv>
        {this.state.dialogBox.dialogBoxState.treeStatus.elements.map((stateObject)=>{
            return setupRenderElement.bind(this)(stateObject);
        })}
    </BodyDiv>
}
function setupRenderElement(stateObject:Object){
    if (stateObject.state.hasChild){
        return setupRenderParent.bind(this)(stateObject);
    }else{
        return (<ElementTagNameAndImgDiv onMouseLeave = {()=>{onMouseLeaveRemoveWebElementMarkScreenShot.bind(this)()}} onMouseEnter={()=>{onMouseEnterGetWebElementMarkScreenShot.bind(this)(stateObject)}} key = {stateObject.key} onContextMenu = {(event)=>{contextMenuClick.bind(this)(event,stateObject)}}>
                    <InspectElementImg src={inspectLogo} alt={"Inspect element"} onClick = {()=>{inspectElement.bind(this)(stateObject)}}/>
                    <ElementTagName>{setupNodeText(stateObject,false)} </ElementTagName>
                </ElementTagNameAndImgDiv>)
    }
}
function setupRenderParent(parentStateObject:Object){
    const ParentDiv = ParentElementDiv(parentStateObject.styleParameter);
    const childrenNodes = parentStateObject.state.childNodes;
    const ChildrenDiv = ParentChildrenDiv(parentStateObject);
    const subComponent = (
        <ParentDiv onMouseLeave = {()=>{onMouseLeaveRemoveWebElementMarkScreenShot.bind(this)()}} onMouseEnter={()=>{onMouseEnterGetWebElementMarkScreenShot.bind(this)(parentStateObject)}} key={parentStateObject.key}>
            <ElementTagNameAndImgDiv onContextMenu = {(event)=>{contextMenuClick.bind(this)(event,parentStateObject)}}>
                <InspectElementImg src={inspectLogo} alt={"Inspect element"} onClick = {()=>{inspectElement.bind(this)(parentStateObject)}}/>
                <ElementTagName onClick = {(event)=>{expandChildrenDiv.bind(this)(event,parentStateObject)}} > {setupNodeText(parentStateObject,true)} </ElementTagName>
            </ElementTagNameAndImgDiv>
            <ChildrenDiv>
                {childrenNodes.map((child)=>{
                    return setupRenderElement.bind(this)(child);
                })}
            </ChildrenDiv>
        </ParentDiv>
    );
    return subComponent;
}
function updateHtmlStyleParameters(styleParam:HtmlPageViewerParentDivStyleParam){
    const newStyleParamObject = {...styleParam};
    newStyleParamObject.marginLeft +=8;
    return newStyleParamObject;
}

function setupNodeText(objectElement:Object){
    let elementText = "<";
    if (objectElement.state.expandState.expanded){
        elementText = elementText.concat("⇣ ")
    }else if (objectElement.state.expandState.expandable){
        elementText = elementText.concat("⇢ ");
    }
    const elementIdent = `${objectElement.domElement.tagName.toUpperCase()} classList=${objectElement.domElement.classList}   id=${objectElement.domElement.id} />`;
    elementText = elementText.concat(elementIdent);
    return elementText;
}


function contextMenuClick(event:Object,objectElement:Object){
    event.preventDefault();
    const rect = event.target.getBoundingClientRect();
    const optionsProps = createContextMenuOptions.bind(this)(objectElement);
    const display = {x:rect.left + rect.width,y:rect.top};
    this.contextEventClick({options:optionsProps,displayConfig:display});
}
function createContextMenuOptions(domElement:Object){
    const addToParamAssertOption ={name:"Add to assert",callback:()=>{addToParamAssert.bind(this)(domElement)}};
    return [addToParamAssertOption];
}
function addToParamAssert(elementToAssert:Object){
    const listOfWebElementParams = this.state.codeAssertConf.webElementParams.slice();
    const assertElement:ElementToAssertWith = {
        dto:{
            xpath:elementToAssert.xpath,
            tag:elementToAssert.domElement.tagName,
            value:null,
        },
        index:listOfWebElementParams.length,
        assertionAttributes: [],
    };
    listOfWebElementParams.push(assertElement);
    this.setState({...this.state,codeAssertConf:{...this.state.codeAssertConf,webElementParams:listOfWebElementParams,}});
}
function closeContextMenu(){
    this.contextEventOff();
}

function expandChildrenDiv(event:Object,parent:Object){
    const childrenDiv = event.target.parentNode.nextSibling;
    parent.state.expandState.expanded = !parent.state.expandState.expanded;
    event.target.innerHTML = setupNodeText(parent);
    childrenDiv.style.display = (childrenDiv.style.display === "flex")?"none":"flex";
}
function inspectElement(element:Element){
    this.setState({...this.state,dialogBox:{
            ...this.state.dialogBox,dialogBoxState: {
                ...this.state.dialogBox.dialogBoxState,
                inspectElement:{
                    element:element,
                }
            }
        }
    });
}

function onMouseEnterGetWebElementMarkScreenShot(objectElement:Object){
    this.props.sockJs.publish({destination:WebSocketConf.sockJs.topics.controllers.assertionEventController.request.markWebElementOnCurrentRequest,body:objectElement.xpath});
}
function onMouseLeaveRemoveWebElementMarkScreenShot(){
    this.props.sockJs.publish({destination:WebSocketConf.sockJs.topics.controllers.assertionEventController.request.getWebScreenRequest,body:""});
}


function getXPathForElement(element:Element) {
    const idx = (sib, name) => sib
        ? idx(sib.previousElementSibling, name||sib.localName) + (sib.localName == name)
        : 1;
    const segs = elm => !elm || elm.nodeType !== 1
        ? ['']
        : elm.id.length > 0
            ? [`id('${elm.id}')`]
            : [...segs(elm.parentNode), `${elm.localName.toLowerCase()}[${idx(elm)}]`];
    return segs(element).join('/');
}




function contextActivateCallback(callback:Function){
    this.contextEventClick = (displayConfig:ContextMenuConfigParamsType)=>{callback(displayConfig)};
}
function contextDeactivateCallback(callback:Function){
    this.contextEventOff = ()=>{callback()};
}
function updateByteDisplayArrayCallback(callback:Function){
    this.updateByteDisplayArray = (byteArray:Array)=>{callback(byteArray)};
}

export function closeDialogCallbackFunction(){
    this.props.sockJs.unsubscribe(WebSocketConf.sockJs.topics.controllers.assertionEventController.response.getDocumentBodyToStringResponse);
}
export function renderPageHtmlViewer(){
    if (this.state.dialogBox.dialogBoxState.htmlDoc === undefined){
        activatePageHtmlViewer.bind(this)(this.props.sockJs);
    }else{
        const contextMenu = renderContextMenuComponent(contextActivateCallback.bind(this),contextDeactivateCallback.bind(this));
        const byteImageDisplay = renderByteImageDisplay(updateByteDisplayArrayCallback.bind(this),{alt:"Web screen"});
        return(
            <PageHtmlViewer id = {"assertDialogBoxesHtmlPageViewerIdClosePurpose"} onClick={closeContextMenu.bind(this)}>
                {contextMenu}
                <HtmlActionBar>
                </HtmlActionBar>
                <HtmlPreviewDiv>
                    <ElementPreviewDiv>
                        {renderInspectElement.bind(this)()}
                        <ElementPreviewImageDiv>
                            {byteImageDisplay}
                        </ElementPreviewImageDiv>
                    </ElementPreviewDiv>

                    <BodyTreePreviewDiv>
                        {renderHtmlDoc.bind(this)()}
                    </BodyTreePreviewDiv>

                </HtmlPreviewDiv>
            </PageHtmlViewer>
        )
    }
}