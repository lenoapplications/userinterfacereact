//@flow
//Core
import React,{Component} from "react";
import styled from 'styled-components';


const IsDisplayedDiv = styled.div
    `
    display:flex;
    width:100%;
    height:100%:
    margin:10px;
    `;
const FirstRowAttribute = styled.div
    `
    display:flex;
    margin-top:2px;
    flex-direction:row;
    width:100%;
    height:15%;
    overflow:hidden;
    `;
const WaitTimeForElementToDisplayLabel = styled.label
    `
    width:auto;
    height:15%
    font-size:65%;
    font-weight:bold;
    `;
const WaitTimeForElementToDisplayInput = styled.input
    `
    width:10%;
    height:9%;
    margin-left:2px;
    font-size:50%;
    
    `;

function isWaitTimeValidate(seconds:Number){
    return seconds !== undefined && seconds.toString().length !== 0;
}

function checkIfAttributeExists(attribute:string,defaultValue:string) {
    if (attribute !== undefined) {
        return attribute;
    } else {
        return defaultValue;
    }
}

function onWaitTimeInputOnBlur(event){
    this.setState({
        ...this.state,assertConf: {
            ...this.state.assertConf,configuration: {
                ...this.state.assertConf.configuration,attributes: {
                    ...this.state.assertConf.configuration.attributes,waitTime:event.target.value
                }
            }
        }
    })
}
export function isDisplayedValidationConfiguration(){
    if (!isWaitTimeValidate(this.state.assertConf.configuration.attributes.waitTime)){
        return false;
    }
    return true;
}

export function renderIsDisplayedComponentConf(){
    return(
      <IsDisplayedDiv>
          <FirstRowAttribute>
              <WaitTimeForElementToDisplayLabel htmlFor = {"waitTimeForElementToDisplayAssertEvent"}>Wait time:</WaitTimeForElementToDisplayLabel>
              <WaitTimeForElementToDisplayInput id = {"waitTimeForElementToDisplayAssertEvent"} placeholder = {checkIfAttributeExists(this.state.assertConf.configuration.attributes.waitTime,"")} type = {"number"} onBlur = {onWaitTimeInputOnBlur.bind(this)}/>
          </FirstRowAttribute>
      </IsDisplayedDiv>
    )
}