//@flow
import React from "react";
import {WebSocketConf} from "../../../../../../../../../env/webSocketConf/WebSocketConf";
import {Client} from '@stomp/stompjs'
//Styled Components
import {ElementDiv,ElementTag,ElementMainAssertConfigurationDiv,ElementAssertConfigurationTypeSelectDiv,ElementAssertConfigurationSelect,ElementAssertConfigurationOption
,AssertElementParagraph,ElementHeaderDiv,ElementBodyInfoDiv,ElementAssertionConfListMainDiv,
ElementAssertionConfListTypeAssertion,ElementAssertionConfListTypeAssertionName} from "./AssertionDialogStyleComponents";
//Types
import type {NewEventAssertPayload} from "../../../../../../../../types/store/reducers/recorderReducers/ActiveRecorderActionsType";
import {ElementToAssertWith} from "../../../../../../../../types/recorder/assertions/AssertionsAttributes";
//Parts
//Functions
import {isDisplayedValidationConfiguration} from "./parts/assertionComponent/isDisplayedComponent";
import {
    isCodeAssertValidationConfiguration,
    renderCodeAssertComponentConf
} from "./parts/assertionComponent/CodeAssertComponent";


export function activateAssertEventOnServer(client:Client,eventObject:Object){
    const stringJsonEvent = JSON.stringify(eventObject);
    client.publish({destination:WebSocketConf.sockJs.topics.controllers.assertionEventController.request.activateAssertOnEventRequest,body:stringJsonEvent});
}
export function setupSubscriptionTo(client:Client,callback:Function){
    client.subscribe(WebSocketConf.sockJs.topics.controllers.assertionEventController.response.assertEventRecvResponse,(message)=> {
        const assertEvent = JSON.parse(message.body);
        const assertEventObject : ElementToAssertWith= {
            dto:assertEvent,
            index:this.state.assertEvents.list.length,
            assertionAttributes:[],
        };
        callback.bind(this)(assertEventObject);
    });
}
export function setupUnSubscriptionTo(client:Client){
    client.unsubscribe(WebSocketConf.sockJs.topics.controllers.assertionEventController.response.assertEventRecvResponse);
}
export function deactivateAssertEventOnServer(client:Client){
    client.publish({destination:WebSocketConf.sockJs.topics.controllers.assertionEventController.request.deactivateAssertOnEventRequest,body:{}});
    resetCompleteAssertion.bind(this)();
}

export function saveListOfAssertConfiguration(client:Client,assertionAttributes:Array,assertObjectsXpaths:Array){
    const stringify = JSON.stringify({
        xpaths:assertObjectsXpaths,
        assertionTypeList:assertionAttributes
    });
    client.publish({destination:WebSocketConf.sockJs.topics.controllers.assertionEventController.request.saveAssertAttributeListRequest,body:stringify})
}

export function changeAssertionType(type:String){
    this.setState({...this.state,assertionType:type});
}

export function renderAssertComponent(){
    switch (this.state.assertionType) {
        case "CODE":return renderCodeAssertComponentConf.bind(this)();
    }
}



function resetCompleteAssertion(){
    this.setState({
        status:{
            assertActivated : false,
        },
        assertEvents:{
            list:[],
        },
        assertConf:{
            currentAssertObject : -1,
            configuration:{
                type:"Choose Assert type",
                attributes:{},
            }
        }
    });
}

