//@flow
//Core
import React from "react";
import styled from 'styled-components';
import AceEditor from 'react-ace';
import 'brace/mode/html';
import 'brace/theme/monokai';
import 'brace/mode/java'
import 'brace/ext/language_tools';
//Functions
import {saveListOfAssertConfiguration, setupSubscriptionTo} from "../../AssertionDialogBehavior";
//Types
import type {ElementToAssertWith} from "../../../../../../../../../../types/recorder/assertions/AssertionsAttributes";


const CodeAssertDiv = styled.div
    `
    display:flex;
    flex-direction:column;
    width:100%;
    height:100%;
    overflow:hidden;
    `;

const CodeAssertActionBarDiv = styled.div
    `
    display:flex;
    width:100%;
    height:5%;
    overflow:hidden;
    `;
const CodeAssertActionBarButton = styled.button
    `
    height:70%;
    margin-left:auto;
    font-size:50%;
    align-self:center;
    font-weight:bold;
    `;
const CodeAssertBodyDiv = styled.div
    `
    display:flex;
    width:100%;
    height:90%;
    overflow:hidden;
    `;
const CodeAssertLeftDiv = styled.div
    `
    display:flex;
    flex-direction:column;
    width:20%;
    height:100%;
    border-top:2px outset gray;
    border-right:2px outset gray;
    `;
const CodeAssertRightDiv = styled.div
    `
    display:flex;
    flex-direction:column;
    width:20%;
    height:100%;
    border-top:2px outset gray;
    border-right:2px outset gray;
    `;
const CodeAssertHtmlPreviewDialogButton = styled.button
    `
    height:50%;
    font-size:50%;
    font-weight:bold;
    align-self:flex-end;
    `;
const CodeAssertCodeEditor = styled.div
    `
    display:flex;
    width:60%;
    height:100%;
    overflow:hidden;
    background-color:yellow;
    `;




function initCodeAssert(){
    if (this.state.codeAssertConf === undefined){
        setupSubscriptionTo.bind(this)(this.props.sockJs,serverSubscribeCallback);
        createCodeConfInState.bind(this)();
    }
}
function createCodeConfInState(){
    this.setState({...this.state,codeAssertConf:{
            webElementParams:[],
            codeAttributes:{
                type:"codeAssert",
                attributes:{
                    code: {
                        implementation: "",
                    }
                }
            },
        }
    });
}
function getParameterParam(webElementParams:Array){
    const codeParams = createCodeParam(webElementParams);
    return `
    //Assert function
    //params: 
    //  -webElement - selenium Object
    //Do not change the template
    {${codeParams}`
}
function createCodeParam(webElementParams:Array){
    if (webElementParams.length === 0){
        return ""
    }else{
        let stringParam = "";
        webElementParams.map((param,index)=>{
            if (index === 0 && webElementParams.length === 1){
                stringParam = `${param.dto.tag}${index}->`;
            }
            else if (index === webElementParams.length-1){
               stringParam = stringParam.concat(`${param.dto.tag}${index}->`);
            }else{
                stringParam = stringParam.concat(`${param.dto.tag}${index},`);
            }
        });
        return stringParam;
    }
}
function createFullCode(){
    return `${getParameterParam(this.state.codeAssertConf.webElementParams)}
    ${this.state.codeAssertConf.codeAttributes.attributes.code.implementation}
    }`;
}
function serverSubscribeCallback(elementToAssertWith:ElementToAssertWith){
    const listOfWebElementParams = this.state.codeAssertConf.webElementParams.slice();
    listOfWebElementParams.push(elementToAssertWith);
    this.setState({...this.state,codeAssertConf:{
                    ...this.state.codeAssertConf,webElementParams:listOfWebElementParams,
        }
    });
}

function onCodeChange(event,editor){
    this.setState({
        ...this.state,codeAssertConf:{
            ...this.state.codeAssertConf,codeAttributes:{
                ...this.state.codeAssertConf.codeAttributes,attributes:{
                    ...this.state.codeAssertConf.codeAttributes.attributes,code:{
                        ...this.state.codeAssertConf.codeAttributes.attributes.code,implementation:parseImplementationFromMainFunction.bind(this)(editor.getValue()),
                    }
                }
            }
        }
    })
}
function parseImplementationFromMainFunction(editorValue:String){
    let implementationCode = editorValue.replace(getParameterParam(this.state.codeAssertConf.webElementParams),"");
    const firstNewLine = implementationCode.indexOf("\n") + 1;
    const lastNewLine = implementationCode.lastIndexOf("\n");
    implementationCode = implementationCode.substr(0,implementationCode.lastIndexOf("}"));
    implementationCode = implementationCode.substr(0,lastNewLine);
    implementationCode = implementationCode.substr(firstNewLine);
    implementationCode = "  ".concat(implementationCode.trim());
    return implementationCode;
}

function saveAssert(){
    const listofXpaths = [];
    this.state.codeAssertConf.webElementParams.map((element)=>{listofXpaths.push(element.dto.xpath); });
    const codeAttributes = {...this.state.codeAssertConf.codeAttributes};
    codeAttributes.attributes.code = createFullCode.bind(this)();
    saveListOfAssertConfiguration(this.props.sockJs,[codeAttributes],listofXpaths);
}

function getAutoCompleteForWebElement(){
    const maps = [
        {name:"getElement",description:"users in the system"},
        {name:'isDisplayed',description:"users in the syste"},
    ];
    return{
        getCompletions:(editor,session,pos,prefix,callback)=>{
            callback(null,maps.map((mape)=>{
                return {name: "methodName", value: "methodName", score: 1, meta: "returnType", description: "description", parameters: "s.parameters", returnType: "returnType"}
            }))
        }

    }
}


export function renderCodeAssertComponentConf(){
    initCodeAssert.bind(this)();
    let code = "";
    if (this.state.codeAssertConf !== undefined) code = createFullCode.bind(this)();
    return(
        <CodeAssertDiv>
            <CodeAssertActionBarDiv>
                <CodeAssertHtmlPreviewDialogButton onClick={()=>{this.setState({...this.state,dialogBox:{active:true,type:"PageHtmlViewer",dialogBoxState:{}}})}}>HtmlViewer</CodeAssertHtmlPreviewDialogButton>
                <CodeAssertActionBarButton onClick = {saveAssert.bind(this)}>Save assert</CodeAssertActionBarButton>
            </CodeAssertActionBarDiv>

            <CodeAssertBodyDiv>
                <CodeAssertLeftDiv>
                </CodeAssertLeftDiv>

                <CodeAssertCodeEditor>
                    <AceEditor
                        width="100%"
                        padding="2px"
                        height="100%"
                        ref = "aceEditor"
                        mode="java"
                        theme="monokai"
                        name="AssertionEditor"
                        fontSize={10}
                        showPrintMargin={true}
                        showGutter={true}
                        highlightActiveLine={true}
                        value = {code}
                        onBlur = {onCodeChange.bind(this)}
                        setOptions={{
                            enableBasicAutocompletion: [getAutoCompleteForWebElement()],
                            enableLiveAutocompletion: [getAutoCompleteForWebElement()],
                            enableSnippets: false,
                            showLineNumbers: true,
                            tabSize: 2,}}/>
                </CodeAssertCodeEditor>

                <CodeAssertRightDiv>

                </CodeAssertRightDiv>

            </CodeAssertBodyDiv>

        </CodeAssertDiv>
    )
}