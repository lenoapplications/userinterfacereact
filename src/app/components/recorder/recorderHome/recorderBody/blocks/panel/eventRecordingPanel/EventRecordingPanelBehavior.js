//@flow
import React from "react";

//Env
import {WebSocketConf} from "../../../../../../../../env/webSocketConf/WebSocketConf";
//Types
import type {NewEventAssertPayload} from "../../../../../../../types/store/reducers/recorderReducers/ActiveRecorderActionsType";
import ActiveRecorderAction from "../../../../../../../reducers/recorder/activeRecording/actions/ActiveRecorderActionTypes";
import type {DocumentEventDto} from "../../../../../../../types/dto/serializableEvents/DocumentEventDto";
import type {WindowEventDto} from "../../../../../../../types/dto/serializableEvents/WindowEventDto";
import ActiverRecorderActionsTypes from '../../../../../../../reducers/recorder/activeRecording/actions/ActiveRecorderActionTypes';

//Styled Components
import {ListItemDiv,ItemDivEventInfoDiv,ItemDivEventProperties,EventStep,EventInfo
,AssertionButton,ItemDivEventAssertion} from "./EventRecordingPanelStyleComponents";



export function setupSubscriptionTo(dispatch:Function,client:Client){
    client.subscribe(WebSocketConf.sockJs.topics.controllers.eventsController.response.eventRecvResponse,(message)=>{
        const response = JSON.parse(message.body);
        dispatch({
            type:ActiveRecorderAction.events.NEW_EVENT_RECV,
            payload:response,
        })
    });
}

export function assertionDialog(eventObject:Object,index:Number){
    const newEventAssertAction:NewEventAssertPayload = {
        type:ActiverRecorderActionsTypes.events.assertions.EVENT_TO_ASSERT,
        payload:{
            index:index,
            eventObject:eventObject,
        }
    };

    this.setState({assertionDialog:displayStatus});

    this.props.newAssertionEvent(newEventAssertAction);
    const displayStatus = (this.state.assertions.assertionDialog !== true);
    this.setState({...this.state,assertions:{
            ...this.state.assertions,assertionDialog:displayStatus
        }
    });
}


export function renderListOfEvents(){
    const assertionFunctionDisplayBinded = assertionDialog.bind(this);
    return this.props.eventsList.map((event,index)=> {
         return (event.eventOn === "document")?documentEventListeners(event,index,assertionFunctionDisplayBinded):windowEventListeners(event,index,assertionFunctionDisplayBinded);
        }
    );
}

export function saveRecord(client:Client){
    client.publish({destination:WebSocketConf.sockJs.topics.controllers.applicationSetupController.request.saveRecordRequest,body:{}})
    console.log("poslano");
}


function documentEventListeners(event:DocumentEventDto, index:Number, assertionDisplayFunction:Function){
    const recordNumber = "Step ".concat(index.toString()).concat(".");
    return (
        <ListItemDiv key = {recordNumber}>
            <ItemDivEventInfoDiv>
                <EventStep>{recordNumber}</EventStep>
                <EventInfo>{event.event + "  "+event.tag}</EventInfo>
            </ItemDivEventInfoDiv>

            <ItemDivEventProperties>

            </ItemDivEventProperties>

            <ItemDivEventAssertion>
                <AssertionButton onClick ={()=>{assertionDisplayFunction(event,index)}}>ASSERTION</AssertionButton>
            </ItemDivEventAssertion>
        </ListItemDiv>
    );
}
function windowEventListeners(event:WindowEventDto,index:Number,assertionDisplayFunction:Function){
    return (
        <ListItemDiv>
            <p>{event.event}</p>
            <AssertionButton onClick ={()=>{assertionDisplayFunction(event,index)}}>ASSERTION</AssertionButton>
        </ListItemDiv>
    );
}







