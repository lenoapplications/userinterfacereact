//@flow
//Core
import React,{Component} from "react";
import styled from 'styled-components';
import { connect } from 'react-redux';
import {BrowserRouter, Switch} from "react-router-dom";
import {Route} from "react-router";
import {bindActionCreators} from 'redux';
import type {Store} from "../types/store/Store";

//Types
import {ContextMenuOptionType,ContextMenuConfigParamsType} from "../../../types/reusableComponent/ContextMenuTypes";

const createContextMenuDiv =(state)=> {
    return styled.div`
    display: ${state.display?'flex':'none'}; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 10; /* Sit on top */
    left:${state.config.displayConfig.x}px;
    top:${state.config.displayConfig.y}px;
    width:120px;
    `;
};
const OptionDiv = styled.div
    `
    display:flex;
    flex-wrap:wrap;
    width:100%;
    height:auto;
    overflow:hidden;
    `;
const OptionButton = styled.button
    `
    width:100%;
    font-size:50%;
    color:black;
    `;




export class ContextMenu extends Component{
    constructor(props) {
        super(props);
        this.state = {
            display:false,
            config:{
                options:[],
                displayConfig:{
                    x:0,
                    y:0,
                }
            }
        };
        console.log(this.props);
        this.props.activateCallback(this.displayContext.bind(this));
        this.props.deactivateCallback(this.removeContext.bind(this));
    }

    displayContext(contextMenuConfigParamsType:ContextMenuConfigParamsType){
        this.setState({...this.state,display:true,config:contextMenuConfigParamsType})
    }
    removeContext(){
        this.setState({
            display:false,
            config:{
                options:[],
                displayConfig:{
                    x:0,
                    y:0,
                }
            }
        });
    }

    render(): React.ReactNode {
        const ContextDiv = createContextMenuDiv(this.state);
        return (
           <ContextDiv>
               {this.state.config.options.map((object,index)=>{
                   const name = object.name;
                   const callback = object.callback;
                   return(<OptionDiv key = {index}>
                                <OptionButton onClick={callback}>{name}</OptionButton>
                          </OptionDiv>)
               })}
           </ContextDiv>
        );
    }

}

export function renderContextMenuComponent(activateCallback:Function,deactivateCallback:Function){
     return <ContextMenu activateCallback = {activateCallback} deactivateCallback = {deactivateCallback}/>;
}