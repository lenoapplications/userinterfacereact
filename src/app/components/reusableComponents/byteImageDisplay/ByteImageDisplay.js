//@flow
//Core
import React,{Component} from "react";
import styled from 'styled-components';
import { connect } from 'react-redux';
import {BrowserRouter, Switch} from "react-router-dom";
import {Route} from "react-router";
import {bindActionCreators} from 'redux';
import type {ByteImageDisplayConfig} from "../../../types/reusableComponent/ByteImageDisplay";


const createDisplayDiv =(state)=> {
    return styled.div`
    display: ${state.display?'flex':'none'}; /* Hidden by default */
    width:100%;
    height:100%;
    overflow:hidden;
    `;
};
const Img = styled.img
    `
    width:100%;
    height:100%;
    `;

class ByteImageDisplay extends Component{
    constructor(props){
        super(props);
        this.state = {
            display:true,
            config:this.props.config,
            data:{
                byteArray:[],
            }
        };
        this.props.updateByteArrayCallback(this.updateByteArray.bind(this));
    }

    updateByteArray(byteArray:Array){
        this.setState({...this.state,data:{
                ...this.state.data,byteArray:byteArray
            }
        });
    }

    render(): React.ReactNode {
        const DisplayDiv = createDisplayDiv(this.state);
        const base64Data = btoa(String.fromCharCode.apply(null, this.state.data.byteArray));
        return (<DisplayDiv>
                    <Img alt = {this.state.config.alt} src = {`data:image/png;base64,${base64Data}`}/>
                </DisplayDiv>);
    }
}


export function renderByteImageDisplay(updateByteArrayCallback:Function,config:ByteImageDisplayConfig){
    return <ByteImageDisplay config = {config} updateByteArrayCallback = {updateByteArrayCallback}/>;
}