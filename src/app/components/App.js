//@flow
//Core
import React,{Component} from "react";
import styled from 'styled-components';
import { connect } from 'react-redux';
import {BrowserRouter, Switch} from "react-router-dom";
import {Route} from "react-router";
import {bindActionCreators} from 'redux';
import type {Store} from "../types/store/Store";
import SockJsClient from 'react-stomp';

//Components
import StartApp from './startApp/StartApp';
import RecorderHome from './recorder/recorderHome/RecorderHome';


const AppBody = styled.div
    `
    width:100%;
    height:100%;
    display:flex;
    overflow:hidden;    
    `;

class App extends Component{
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <AppBody>
                {this.props.connectionStatus === false ?
                    (<StartApp/>)
                    :
                    (<RecorderHome/>)
                }

            </AppBody>
        );
    }
}
function mapStateToProps(store:Store){
    return{
        connectionStatus: store.webSocketReducer.socketStatus.connectedToServer,
    }
}
function mapDispatchToProps(dispatch : Function){
    return {
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(App);