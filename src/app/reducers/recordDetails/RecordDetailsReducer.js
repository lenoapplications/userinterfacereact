//@flow

import {RecordDetailsReducerType} from "../../types/store/reducers/recordDetails/RecordDetailsReducerType";
import type {Action} from "../../types/store/reducers/Action";
import ActionTypes from './actions/RecordDetailActionTypes';

const initialState : RecordDetailsReducerType = {
    path:"nista",
};

function newFileRecordToDisplay(state:RecordDetailsReducerType,action:Action){
    return{...state,path: action.payload}

}
export default function(state:RecordDetailsReducerType = initialState,action:Action){
    switch (action.type) {
        case ActionTypes.newFileRecordToDisplay:return newFileRecordToDisplay(state,action);
        default:return state;
    }
}