//@flow
import {WebSocketReducer} from "../../types/store/reducers/websocketReducer/WebSocketReducerType";
import {AddSockJsRef,ConnectionStatus} from "../../types/store/reducers/websocketReducer/WebSocketActions";
import ActionTypes from "./actions/ActionsTypes";


const initialState : WebSocketReducer = {
    socketConfiguration:{
        url:"ws://localhost:8080/react",
        port:8080,
    },
    socketStatus:{
        connectedToServer:false,
        connectResponseFromServer:"no response"
    },
    sockJs:{
      clientSocket:undefined,
    }
};


function addSocketJsRef(state:WebSocketReducer,action:AddSockJsRef):WebSocketReducer{
    return {...state,sockJs: {
           ...state.sockJs,clientSocket: action.payload
        }
    }
}
function changeConnectionStatus(state:WebSocketReducer,action:ConnectionStatus){
    return {...state,socketStatus: {
            ...state.socketStatus,connectedToServer: action.payload.bool,connectResponseFromServer: action.payload.response
        }
    }
}



export default function(state:WebSocketReducer = initialState, action:Action) : WebSocketReducer{
    switch (action.type) {

        case ActionTypes.connect.ADD_SOCKJS_REF:
            return addSocketJsRef(state,action);
        case ActionTypes.connect.CHANGE_CONNECTION_STATUS:
            return changeConnectionStatus(state,action);

        default:
            return state;
    }
}