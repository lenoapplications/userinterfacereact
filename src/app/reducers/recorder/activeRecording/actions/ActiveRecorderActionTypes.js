export default {
    recordingInit:{
        RECORDING_ACTIVATED:"RcrdActv",
    },
    events:{
        NEW_EVENT_RECV:"NwRcvEvent",
        assertions:{
            EVENT_TO_ASSERT:"EvntTAssrt",
        }
    }
}