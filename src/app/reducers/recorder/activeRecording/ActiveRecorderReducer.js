//@flow
import {ActiveRecorderReducerType} from "../../../types/store/reducers/recorderReducers/ActiveRecorderReducerType";
import ActionTypes from './actions/ActiveRecorderActionTypes';

const initialState : ActiveRecorderReducerType = {
    status:{
        recording:false,
    },
    events:{
        listOfCurrentEvents:[],
        assertions:{
            eventToAssert:{
                index:-1,
                eventObject:undefined,
            }
        }
    }
};


function recordingActivated(state:ActiveRecorderReducerType,action:Action){
    return {...state,status:{
                ...state.status,recording:action.payload}
    };
}
function eventToAssert(state:ActiveRecorderReducerType,action:Action){
    return {...state,events:{
                ...state.events,assertions:{
                    ...state.events.asssertions,eventToAssert:action.payload
            }
        }
    }
}


function newEventRecv(state:ActiveRecorderReducerType,action:Action){
    const list = state.events.listOfCurrentEvents.slice();
    list.push(action.payload);
    return {
        ...state,events:{
            ...state.events,listOfCurrentEvents: list,
        }
    }
}


export default function(state:ActiveRecorderReducerType = initialState, action:Action) : ActiveRecorderReducerType{
    switch (action.type) {

        case ActionTypes.recordingInit.RECORDING_ACTIVATED:
            return recordingActivated(state,action);
        case ActionTypes.events.NEW_EVENT_RECV:
            return newEventRecv(state,action);
        case ActionTypes.events.assertions.EVENT_TO_ASSERT:
            return eventToAssert(state,action);


        default:
            return state;
    }
}