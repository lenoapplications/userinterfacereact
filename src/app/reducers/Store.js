//@flow
import {combineReducers} from "redux";
//reducers

//types
import type{Store} from "../types/store/Store";

//Reducers
import webSocketReducer from "./webSocket/WebSocketReducer";
import activeRecorderReducer from "./recorder/activeRecording/ActiveRecorderReducer";
import componentNavigatorReducer from "./componentNavigator/ComponentNavigatorReducer";
import recordDetailsReducer from './recordDetails/RecordDetailsReducer';
const store: Store = combineReducers({
    webSocketReducer,
    activeRecorderReducer,
    componentNavigatorReducer,
    recordDetailsReducer,
});
export default store;