//@flow

import {ComponentNavigatorReducerType} from "../../types/store/reducers/componentNavigator/ComponentNavigatorReducerType";
import type {Action} from "../../types/store/reducers/Action";
import ActionTypes from './actions/ComponentNavigatorActionTypes';

const initialState : ComponentNavigatorReducerType = {
    recorder:{
        recorderHome:{
            recorderBody:{
                panel:{
                    panelDisplay:"EventRecorder"
                },
                navigation:{
                    navigationDisplay:"RecordsExplorer",
                }
            },
        }
    }

};

//RECORDER BODY ACTIONS
function changePanelDisplay(state:ComponentNavigatorReducerType,action:Action){
    return {...state,recorder:
            {...state.recorder,recorderHome: {
                ...state.recorder.recorderHome, recorderBody: {
                    ...state.recorder.recorderHome.recorderBody,panel:{
                        ...state.recorder.recorderHome.recorderBody.panel,panelDisplay: action.payload
                    }
                }
            }
        }
    }
}
function changeNavigationDisplay(state:ComponentNavigatorReducerType,action:Action){
    return {...state,recorder:
            {...state.recorder,recorderHome: {
                ...state.recorder.recorderHome, recorderBody: {
                    ...state.recorder.recorderHome.recorderBody,navigation:{
                        ...state.recorder.recorderHome.recorderBody.navigation,navigationDisplay: action.payload
                    }
                }
            }
        }
    }
}

function actionOnRecorderBody(state:ComponentNavigatorReducerType,action:Action):ComponentNavigatorReducerType{
    switch (action.type) {
        case ActionTypes.recorder.recorderHome.recorderBody.panel.changePanelDisplay:return changePanelDisplay(state,action);
        case ActionTypes.recorder.recorderHome.recorderBody.navigation.changeNavigationDisplay:return changeNavigationDisplay(state,action);
        default:return null;
    }
}

export default function(state:ComponentNavigatorReducerType = initialState,action:Action){
    var newState = null;
    if ((newState = actionOnRecorderBody(state,action)) !== null){
        return newState;
    }
    return state;
}